def calculate():

    def again():
        calc_again = input('''
        Do you want to convert again?
        Please type Y for YES or N for NO.
        ''')

        if calc_again.upper() == 'Y':
            calculate()
        elif calc_again.upper() == 'N':
            print('See you again.')
        else:
            again()
            
    operation = input('''
    Please type in the option you would like to convert:
    1 for usd to peso
    2 for peso to usd
    3 for euro to peso
    4 for peso to euro
    5 for usd to euro
    6 for euro to usd
    ''')


    my_list= [52.1811, 0.019, 58.412, 0.017, 0.8912, 1.121]

    if operation == '1':
        a=float(input("usd= ")) 
        print("peso: ",a*my_list[0])
    
    elif operation == '2':
        b=float(input("peso= ")) 
        print("usd: ",b*my_list[1])
    
    elif operation == '3':
        c=float(input("euro= ")) 
        print("peso: ",c*my_list[2])

    elif operation == '4':
        d=float(input("peso= ")) 
        print("euro: ",d*my_list[3])

    elif operation == '5':
        e=float(input("usd= ")) 
        print("euro: ",e*my_list[4])
        
    elif operation == '6':
        f=float(input("euro= ")) 
        print("usd: ",f*my_list[5])

    else:
        print('your input is invalid.')

    # Add again() function to calculate() function
    again()

calculate()
